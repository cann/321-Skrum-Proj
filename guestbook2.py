from google.appengine.api import users
from google.appengine.ext import ndb
import webapp2
import jinja2
import urllib2
import urllib
import os
import logging
import json

    
    
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__),'templates')),#allows python to access templates folder
    extensions=['jinja2.ext.autoescape'])


class MainHandler(webapp2.RequestHandler):
    
    def get(self):
        user = users.get_current_user()
        logout_url = users.create_logout_url(self.request.path)
        
        
        if user:
        	template = JINJA_ENVIRONMENT.get_template('booksAPI.html')
        	template_values = {
            	'user': user.nickname(),
            	'url_logout': logout_url,
            	'url_logout_text': 'Log out',
        	}
            
        	self.response.write(template.render(template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri)) 
            
    def post(self):
        
        user = users.get_current_user()
        logout_url = users.create_logout_url(self.request.path)
        
        searchBar = self.request.get("searchBar")
        
        search = "intitle:" + searchBar#"harry%20potter%20and%20the%20goblet%20of%20fire"
        
        #self.response.write(searchBar) test to check search bar text
        
        values = {"q": search}
        url_values = urllib.urlencode(values)
        
        books_api = 'https://www.googleapis.com/books/v1/volumes/?' + url_values
        
        
        response = urllib2.urlopen(books_api).read()
        data = json.loads(response.decode('utf8'))
        items = data['items']
        
        if user:
        	template = JINJA_ENVIRONMENT.get_template('booksAPI.html')
        	template_values = {
            	'user': user.nickname(),
            	'url_logout': logout_url,
            	'url_logout_text': 'Log out',
                'items': items
        	}
            
        	self.response.write(template.render(template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri))
            
            
application = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)